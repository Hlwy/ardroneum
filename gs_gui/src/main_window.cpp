/**
 * @file /src/main_window.cpp
 *
 * @brief Implementation for the qt gui.
 *
 * @date February 2011
 **/
/*****************************************************************************
** Includes
*****************************************************************************/

//#include <QtGui>
#include <QtWidgets>
#include <QMessageBox>
#include <iostream>
#include "../include/gs_gui/main_window.hpp"
#include <cv_bridge/cv_bridge.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <sensor_msgs/image_encodings.h>

/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace gs_gui {

using namespace Qt;

/*****************************************************************************
** Implementation [MainWindow]
*****************************************************************************/

MainWindow::MainWindow(int argc, char** argv, QWidget *parent)
	: QMainWindow(parent)
	, qnode(argc,argv)
{
	ui.setupUi(this); // Calling this incidentally connects all ui's triggers to on_...() callbacks in this class.
    QObject::connect(ui.actionAbout_Qt, SIGNAL(triggered(bool)), qApp, SLOT(aboutQt())); // qApp is a global variable for the application

	setWindowIcon(QIcon(":/images/icon.png"));
	ui.tab_manager->setCurrentIndex(0); // ensure the first tab is showing - qt-designer should have this already hardwired, but often loses it (settings?).

    /*********************
    ** Auto Start
    **********************/
    if ( !qnode.init() )
        showNoMasterMessage();

    qRegisterMetaType<QImage>("QImage");
    qRegisterMetaType<ardrone_autonomy::Navdata>("ardrone_autonomy::Navdata");
    qRegisterMetaType<ardrone_autonomy::navdata_magneto>("ardrone_autonomy::navdata_magneto");
    qRegisterMetaType<geometry_msgs::PoseStamped>("geometry_msgs::PoseStamped");
    qRegisterMetaType<sensor_msgs::ImageConstPtr>("sensor_msgs::ImageConstPtr");


    connect(&qnode, SIGNAL(rangesChanged(double, double, double))
                , this, SLOT(rangesChanged(double, double, double)));
    connect(&qnode, SIGNAL(navDataChanged(ardrone_autonomy::Navdata))
                , this, SLOT(navDataChanged(ardrone_autonomy::Navdata)));
    connect(&qnode, SIGNAL(cameraImageChanged(const sensor_msgs::ImageConstPtr &))
                , this, SLOT(cameraImageChanged(const sensor_msgs::ImageConstPtr &)));
    connect(&qnode, SIGNAL(navDataMagnetoChanged(ardrone_autonomy::navdata_magneto))
                , this, SLOT(navDataMagnetChanged(ardrone_autonomy::navdata_magneto)));
    connect(&qnode, SIGNAL(poseChanged(geometry_msgs::PoseStamped))
                , this, SLOT(poseChanged(geometry_msgs::PoseStamped)));

}

MainWindow::~MainWindow() {}

/*****************************************************************************
** Implementation [Slots]
*****************************************************************************/

void MainWindow::showNoMasterMessage() {
	QMessageBox msgBox;
	msgBox.setText("Couldn't find the ros master.");
	msgBox.exec();
    close();
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    geometry_msgs::Twist msg;
    msg.linear.x = 0.0;
    msg.linear.y = 0.0;
    msg.linear.z = 0.0;
    msg.angular.x = 0.0;
    msg.angular.y = 0.0;
    msg.angular.z = 0.0;

    switch (event->key())
    {
    case Qt::Key_W:
        ui.labelCmd->setText("Forward");
        msg.linear.x = 0.35;
        qnode.sendManualControl(msg);
        break;
    case Qt::Key_S:
        ui.labelCmd->setText("Backward");
        msg.linear.x = -0.35;
        qnode.sendManualControl(msg);
        break;
    case Qt::Key_A:
        ui.labelCmd->setText("Left");
        msg.linear.y = 0.35;
        qnode.sendManualControl(msg);
        break;
    case Qt::Key_D:
        ui.labelCmd->setText("Right");
        msg.linear.y = -0.35;
        qnode.sendManualControl(msg);
        break;
    case Qt::Key_Q:
        ui.labelCmd->setText("Turn AntiClockwise");
        msg.angular.z = 0.5;
        qnode.sendManualControl(msg);
        break;
    case Qt::Key_E:
        ui.labelCmd->setText("Turn Clockwise");
        msg.angular.z = -0.5;
        qnode.sendManualControl(msg);
        break;
    case Qt::Key_H:
        ui.labelCmd->setText("Hover");
        qnode.sendManualControl(msg);
        break;
    case Qt::Key_R:
        ui.labelCmd->setText("Start");
        qnode.sendStartMsg();
        break;
    case Qt::Key_L:
        ui.labelCmd->setText("Land");
        qnode.sendLandMsg();
        break;
    case Qt::Key_T:
        ui.labelCmd->setText("Auto");
        qnode.sendAutoControlMsg();
        break;
    }

}

/*****************************************************************************
** Implementation [Menu]
*****************************************************************************/

void MainWindow::on_actionAbout_triggered() {
    QMessageBox::about(this, tr("About ..."),tr("<h2>AR.Drone Ground Station</h2><p>Copyright Alex Buyval</p>"));
}

void MainWindow::on_button_Start_clicked()
{
    qnode.sendStartMsg();
}

void MainWindow::on_button_Land_clicked()
{
    qnode.sendLandMsg();
}

void MainWindow::on_button_Reset_clicked()
{
    qnode.sendResetMsg();
}

void MainWindow::rangesChanged(double front, double left, double right)
{
    ui.lbFrontRange->setText(QString("%1").arg(front));
    ui.lbLeftRange->setText(QString("%1").arg(left));
    ui.lbRightRange->setText(QString("%1").arg(right));
}

void MainWindow::navDataChanged(ardrone_autonomy::Navdata msg)
{
    //velocities
    ui.lbVX->setText(QString::number(msg.vx,'f',2));
    ui.lbVY->setText(QString::number(msg.vy,'f',2));
    ui.lbVZ->setText(QString::number(msg.vz,'f',2));
    //angles
    ui.lbPitch->setText(QString::number(msg.rotY,'f',2));
    ui.lbRoll->setText(QString::number(msg.rotX,'f',2));
    //accelerations
    ui.lbAccX->setText(QString::number(msg.ax,'f',2));
    ui.lbAccY->setText(QString::number(msg.ay,'f',2));
    ui.lbAccZ->setText(QString::number(msg.az,'f',2));

}

void MainWindow::navDataMagnetoChanged(ardrone_autonomy::navdata_magneto msg)
{
    ui.lbPitch->setText(QString::number(msg.heading_gyro_unwrapped,'f',2));
}

void MainWindow::cameraImageChanged(const sensor_msgs::ImageConstPtr &msg)
{
    try
    {
        // First let cv_bridge do its magic
        cv_bridge::CvImageConstPtr cv_ptr = cv_bridge::toCvShare(msg, sensor_msgs::image_encodings::RGB8);
        conversion_mat_ = cv_ptr->image;
    }
    catch (cv_bridge::Exception& e)
    {
        qWarning("while trying to convert image from '%s' to 'rgb8' an exception was thrown (%s)", msg->encoding.c_str(), e.what());
        return;
    }

    // image must be copied since it uses the conversion_mat_ for storage which is asynchronously overwritten in the next callback invocation
    QImage image(conversion_mat_.data, conversion_mat_.cols, conversion_mat_.rows, conversion_mat_.step[0], QImage::Format_RGB888);

    ui.lbSourceCamera->setPixmap(QPixmap::fromImage(image));
    ui.lbSourceCamera->resize(image.width(), image.height());
}

void MainWindow::poseChanged(geometry_msgs::PoseStamped msg)
{
    ui.lbPosX->setText(QString::number(msg.pose.position.x,'f',2));
    ui.lbPosY->setText(QString::number(msg.pose.position.y,'f',2));
    ui.lbPosZ->setText(QString::number(msg.pose.position.z,'f',2));

}



}  // namespace gs_gui

