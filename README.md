ArdroneUM
=========

UM team's repository for russian competition of fly robots


## How to Run

To launch real AR.Drone use the following command:

`roslaunch navigation ardrone.launch`

or use one from following to launch gazebo simulation:


`roslaunch gazebo_simulation ardrone_gazebo.launch` (main case)

`roslaunch gazebo_simulation ardrone_gazebo_eight.launch` (to test the "eight" path)

`roslaunch particles_filter_bfl ardrone_gazebo_with_pf.launch` (to test the particle filter)


then type the following command to run the flying program:

`rostopic pub /start std_msgs/Empty`