#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/core/core.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <string>
#include <std_msgs/Empty.h>
#include <visual_system/Cross.h>
//#include <opencv/cv.hpp>
//#include <opencv/cxcore.hpp>
//#include <opencv/cvaux.hpp>

using namespace std;

using namespace cv;

namespace enc = sensor_msgs::image_encodings;

static const char WINDOW[] = "Image window";

class ImageConverter
{
	ros::NodeHandle nh_;
	ros::Publisher cross_pub;
	image_transport::ImageTransport it_;
	image_transport::Subscriber image_sub_;
	image_transport::Publisher image_pub_;
	string haar_classifier_filename, video_filename;
	cv::CascadeClassifier cascade_;
	cv::Mat cv_image_out_;
	cv::Mat cv_image_gray_;  /**< Grayscale image */
	double contrast_gain, gauss_sigma, brightness;
	int gauss_core;
	int erosion_size;
	int dilate_size;
	int min_detect_size;

public:
	ImageConverter()
		: it_(nh_)
	{

		//load parameters
		ros::param::get("~haar_classifier_filename", haar_classifier_filename);

		bool use_video = ros::param::get("~video_filename", video_filename);

		contrast_gain = (ros::param::get("~contrast_gain", contrast_gain)) ? contrast_gain : 1.0;
		gauss_core  = (ros::param::get("~gauss_core", gauss_core)) ? gauss_core : 3;
		gauss_sigma  = (ros::param::get("~gauss_sigma", gauss_sigma)) ? gauss_sigma : 1;
		erosion_size  = (ros::param::get("~erosion_size", erosion_size)) ? erosion_size : 3;
		dilate_size  = (ros::param::get("~dilate_size", dilate_size)) ? dilate_size : 2;
		brightness = (ros::param::get("~brightness", brightness)) ? brightness : 0;
		min_detect_size = (ros::param::get("~min_detect_size", min_detect_size)) ? min_detect_size : 45;

		cross_pub = nh_.advertise<visual_system::Cross>("/cross", 1);
		// images_ready_ = 0;
		bool cascade_ok = cascade_.load(haar_classifier_filename);
		if (!cascade_ok)
		{
			ROS_ERROR_STREAM("Cascade file " << haar_classifier_filename << " doesn't exist.");
		}
		cv::namedWindow("Cross detector: Cross Detection", CV_WINDOW_AUTOSIZE);

		if (use_video)
		{
			CaptureVideoFile();
		} else
		{
			  image_sub_ = it_.subscribe("ardrone/image_raw", 1, &ImageConverter::imageCb, this);// прописать топик с коптера
		}


	}

	~ImageConverter()
	{
		cv::destroyWindow(WINDOW);
	}

	void imageCb(const sensor_msgs::ImageConstPtr& msg)
	{

		cv_bridge::CvImageConstPtr image_ptr = cv_bridge::toCvShare(msg,sensor_msgs::image_encodings::BGR8);

		cv::Mat image;
		image = image_ptr->image;

		if (image.channels() == 1) {
			cv_image_gray_.create(image.size(), CV_8UC1);
			image.copyTo(cv_image_gray_);
		}
		else if (image.channels() == 3) {
			cv_image_gray_.create(image.size(), CV_8UC1);
			cv::cvtColor(image, cv_image_gray_, CV_BGR2GRAY);
		}
		else {
			std::cerr << "Unknown image type"<<std::endl;
		}

		DetectCross();

	}

	void DetectCross()
	{

		ros::Time prevTime = ros::Time::now();

		//cv_image_out_ = image.clone();
		//cv::GaussianBlur(cv_image_gray_, cv_image_gray_, cv::Size(gauss_core, gauss_core), gauss_sigma, gauss_sigma);



		cv_image_gray_.convertTo(cv_image_gray_,-1, contrast_gain , brightness);
		resize(cv_image_gray_, cv_image_gray_, Size(0,0), 0.5, 0.5);

		//MORPH_RECT, MORPH_CROSS, MORPH_ELLIPSE


		/*
		cv::Mat element_ero = getStructuringElement( MORPH_RECT,
				Size( 2*erosion_size + 1, 2*erosion_size+1 ),
				Point( erosion_size, erosion_size ) );



		// Apply the erosion operation
		erode( cv_image_gray_, cv_image_gray_, element_ero);

		cv::Mat element_dil = getStructuringElement( MORPH_RECT,
				Size( 2*dilate_size + 1, 2*dilate_size+1 ),
				Point( dilate_size, dilate_size ) );

		dilate(  cv_image_gray_, cv_image_gray_, element_dil );
		*/
		std::vector<cv::Rect> faces_vec;
		cascade_.detectMultiScale( cv_image_gray_, faces_vec, 1.1, 1, CV_HAAR_DO_CANNY_PRUNING|CV_HAAR_FIND_BIGGEST_OBJECT|CV_HAAR_DO_ROUGH_SEARCH , cv::Size(min_detect_size,min_detect_size));

		cv::Rect *r;
		cv::Scalar color;

		for (uint iface = 0; iface < faces_vec.size(); iface++) {
			r = &faces_vec[iface];
			// Visualization by image display.
			color = cv::Scalar(0,255,0);
			cv::rectangle(cv_image_gray_,
					cv::Point(r->x,r->y),
					cv::Point(r->x+r->width, r->y+r->height), color, 4);

			double x_center = r->x+r->width/2;
			double y_center = r->y+r->height/2;

			visual_system::Cross cross_msg;
			cross_msg.header.stamp = ros::Time::now();
			cross_msg.x = x_center/(cv_image_gray_.size().width/2) - 1; //[-1 1]
			cross_msg.y = y_center/(cv_image_gray_.size().height/2) - 1; //[-1 1]
			cross_msg.type = 1;
			//ROS_INFO("Cross x=%.2f y=%.2f width=%d height=%d xnorm=%f ynorm=%f" , x_center, y_center, cv_image_gray_.size().width, cv_image_gray_.size().height, cross_msg.x, cross_msg.y);
			cross_pub.publish(cross_msg);//пуст_сообщение крест

		}

		int fontFace = FONT_HERSHEY_SCRIPT_SIMPLEX;
		double fontScale = 1;
		int thickness = 3;

		std::string text = boost::lexical_cast<std::string>((ros::Time::now() - prevTime).toSec());

		Point textOrg(10,20);

		putText(cv_image_gray_, text, textOrg, fontFace, fontScale, Scalar::all(255), thickness, 8);

		//  ROS_INFO("found %d faces" , faces_vec.size());
		cv::imshow("Cross detector: Cross Detection",cv_image_gray_);
		cv::waitKey(1);

	}

	void CaptureVideoFile()
	{

		VideoCapture cap(video_filename); // open the video file
		if(!cap.isOpened()) // check if we succeeded
		{
			ROS_ERROR_STREAM("Video file " << video_filename << " doesn't exist.");
			return;
		}

		ros::Rate loop_rate(10);

		cv::Mat image;

		while (ros::ok())
		{
			cap >> image;
			cvtColor(image, cv_image_gray_, CV_BGR2GRAY);
			DetectCross();

			ros::spinOnce();
			loop_rate.sleep();

		}

	}

};
// перенести cascade из facedetection faces.cpp

int main(int argc, char** argv)
{
  ros::init(argc, argv, "image_converter");
  ImageConverter ic;
  ros::spin();
  return 0;
}
