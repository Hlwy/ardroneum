/* Code for Arduino Uno R3 
Assumes the sensor is using the default address 
Sensor Connections: 
Pin 7 to GND 
Pin 6 to 5V 
Pin 5 to SCL 
Pin 4 to SDA 
Requires pull-ups for SCL and SDA connected to 5V to work reliably 
*/ 
#include "Wire.h" 
//The Arduino Wire library uses the 7-bit version of the address, so the code example uses 0x70 instead of the 8-bit 0xE0 
#define SensorAddress_1 byte(0x77) // Адрес первого дальномера
#define SensorAddress_2 byte(0x79) // Адрес второго дальномера
#define SensorAddress_3 byte(0x78) // Адрес третьего дальномера

//The sensors ranging command has a value of 0x51 
#define RangeCommand byte(0x51) 
 
char data_1[100];
char data_2[100];
char data_3[100];
 
void setup() { 
 Serial.begin(9600); //Open serial connection at 9600 baud 
 Wire.begin(); //Initiate Wire library for I2C communications with the I2CXL-MaxSonar-EZ 
} 
void loop(){ 
 takeRangeReading(SensorAddress_1); //Tell the sensor 1 to perform a ranging cycle
 takeRangeReading(SensorAddress_2); //Tell the sensor 2 to perform a ranging cycle 
 takeRangeReading(SensorAddress_3); //Tell the sensor 3 to perform a ranging cycle 
 delay(100); //Wait for sensor to finish 
 word range_1 = requestRange(SensorAddress_1); //Get the range from the sensor 1
 word range_2 = requestRange(SensorAddress_2); //Get the range from the sensor 1
 word range_3 = requestRange(SensorAddress_3); //Get the range from the sensor 1
 sprintf(data_1, "%3d", range_1);
 sprintf(data_2, "%3d", range_2);
 sprintf(data_3, "%3d", range_3);
 Serial.print("999;");
 Serial.print(data_1); Serial.print(";"); //Print
 Serial.print("745;"); //Print
 Serial.print(data_2); Serial.print(";"); //Print
 Serial.print(data_3); Serial.print(";"); //Print
 Serial.print("989;");
 Serial.print("\n");
} 
//Commands the sensor to take a range reading 
void takeRangeReading(byte SensorAddress){ 
 Wire.beginTransmission(SensorAddress); //Start addressing
 Wire.write(RangeCommand); //send range command 
 Wire.endTransmission(); //Stop and do something else now 
} 
//Returns the last range that the sensor determined in its last ranging cycle in centimeters. Returns 0 if there is no communication. 
word requestRange(byte SensorAddress){ 
 Wire.requestFrom(SensorAddress, byte(2)); 
 if(Wire.available() >= 2){ //Sensor responded with the two bytes 
 byte HighByte = Wire.read(); //Read the high byte back 
 byte LowByte = Wire.read(); //Read the low byte back 
 word range = word(HighByte, LowByte); //Make a 16-bit word out of the two bytes for the range 
 return range; 
 } 
 else { 
 return word(0); //Else nothing was received, return 0 
 } 
}

void changeAddress(byte oldAddress, byte newAddress, boolean SevenBitHuh){ 
 Wire.beginTransmission(oldAddress); //Begin addressing 
 Wire.write(ChangeAddressCommand1); //Send first change address command 
 Wire.write(ChangeAddressCommand2); //Send second change address command 
 
 byte temp; 
 if(SevenBitHuh){ temp = newAddress << 1; } //The new address must be written to the sensor 
 else { temp = newAddress; } //in the 8bit form, so this handles automatic shifting 
 
 Wire.write(temp); //Send the new address to change to 
 
 Wire.endTransmission(); 
}