

#include <stdlib.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h> /* close() */
#include <string.h> /* memset() */
#include <fcntl.h>
#include <sys/ioctl.h>
#include <termios.h>

// SETUP

// The default serial device to use without options.
// Eg. /dev/ttyPA0 for regular serial port or /dev/ttyUSB0 for USB connection.
//#define DEFAULT_SERIAL_DEVICE   "/dev/ttyUSB0"
#define DEFAULT_SERIAL_DEVICE   "/dev/ttyUSB0"

// OTHER SETTINGS

// Address of control server on drone, should be 192.168.1.1
// Test the server locally by setting address to "127.0.0.1" and 
// typing "nc -l -u 127.0.0.1 5556" in a terminal window!
//#define REMOTE_SERVER_ADDRESS   "192.168.1.3" // Изменено при добавлении адреса в параметры (закомментировано)
//#define REMOTE_SERVER_ADDRESS   "127.0.0.1" 
// Port of control server for AT commands, leave at 5556
#define REMOTE_SERVER_PORT      5560
// Buffer size for AT comands, increase if needed
#define MAX_MESSAGE_LENGTH      100


int main(int argc, char* argv[])
{
    int flags               = 0;    // Send flags, 0 should be good
    int connectionSocket    = 0;
    int returnNo            = 0;    
    uint addressLength      = 0;
    
    //char message[MAX_MESSAGE_LENGTH];
    char* remoteIpAddress   = NULL;
    
    char* adress_server = argv[1]; // Изменено при добавлении адреса в параметры (добавлено)

    struct sockaddr_in remoteAddress;
    
    
    int fd;
    char *portname = "/dev/ttyUSB0";
    /* Open the file descriptor in non-blocking mode */
    fd = open(portname, O_RDWR | O_NOCTTY);
    
    /* Set up the control structure */
    struct termios toptions;
    
    /* Get currently set options for the tty */
    tcgetattr(fd, &toptions);
    
    /* Set custom options */
    
    /* 9600 baud */
    cfsetispeed(&toptions, B9600);
    cfsetospeed(&toptions, B9600);
    /* 8 bits, no parity, no stop bits */
    toptions.c_cflag &= ~PARENB;
    toptions.c_cflag &= ~CSTOPB;
    toptions.c_cflag &= ~CSIZE;
    toptions.c_cflag |= CS8;
    /* no hardware flow control */
    toptions.c_cflag &= ~CRTSCTS;
    /* enable receiver, ignore status lines */
    toptions.c_cflag |= CREAD | CLOCAL;
    /* disable input/output flow control, disable restart chars */
    toptions.c_iflag &= ~(IXON | IXOFF | IXANY);
    /* disable canonical input, disable echo,
    disable visually erase chars,
    disable terminal-generated signals */
    toptions.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    /* disable output processing */
    toptions.c_oflag &= ~OPOST;
    
    /* wait for 25 characters to come in before read returns */
    toptions.c_cc[VMIN] = 25;
    /* no minimum time to wait before read returns */
    toptions.c_cc[VTIME] = 0;
    
    /* commit the options */
    tcsetattr(fd, TCSANOW, &toptions);
    
    /* Wait for the Arduino to reset */
    usleep(1000*1000);
    
    
    // Use default ip address if no ip was given
    if (remoteIpAddress == NULL)
    {
        remoteIpAddress = adress_server;
    }
    
    // Create socket with UDP setting (SOCK_DGRAM)
    connectionSocket = socket(AF_INET, SOCK_DGRAM, 0);
    if (connectionSocket < 0)
    {
	printf("ERROR: Cannot open socket!\n"); // добавлено сообщение об ошибке
        fprintf(stderr,"ERROR: Cannot open socket!\n");
        exit(1);
    }
    
    /*
    int fd,l;
    char buff[1000]="";
    fd=open("/dev/ttyUSB0", O_RDWR | O_NOCTTY);
    */
    // Initialise remote server address
    memset(&remoteAddress, 0, sizeof(struct sockaddr_in));
    remoteAddress.sin_family = AF_INET;
    remoteAddress.sin_addr.s_addr = inet_addr(remoteIpAddress);
    remoteAddress.sin_port = htons(REMOTE_SERVER_PORT);
    addressLength = sizeof(remoteAddress);
    
    // If we reach this point we are up and running!
    printf("Proxy Ready: Waiting for data in %s to send to %s:%u!\n",
           portname,
           inet_ntoa(remoteAddress.sin_addr),
           ntohs(remoteAddress.sin_port));
    char buf[256]; // 256 ориганальное значение
    // Server infinite loop, use ctrl+c to kill proc
    while (1)
    {
	/* Flush anything already in the serial buffer */
	tcflush(fd, TCIFLUSH);
	/* read up to 128 bytes from the fd */
	int n = read(fd, buf, 128); // оригинальное значение
	/*int n = read(fd, buf, 100);   // замена 128 на 100
	if (n<0);      */               // дополнение дабы исключить ошибку не использования объявленной переменной.
	/* print how many bytes read */
	printf("%i bytes got read...\n", n); // ориганальная строка с использованием n
	/* print what's in the buffer */
	printf("Buffer contains...\n%s", buf); // оригинальная строка с выводом содержимого буфера
	//printf("%s", buf); // замена на простой вывод содержимого буфера
            
	    // Send message to remote server
            returnNo = sendto(connectionSocket,
                              buf,
                              strlen(buf),
                              flags,
                              (struct sockaddr *)&remoteAddress,
                              addressLength);
            
            if (returnNo < 0)
            {
	        printf("ERROR: Cannot send data!\n"); // дополнение сообщением об ошибке
                fprintf(stderr, "%s > ERROR: Cannot send data!\n", argv[0]);
            }
//	 }
    }
    // End of server infinite loop
    
    return 0;
}
