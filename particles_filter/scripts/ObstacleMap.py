#!/usr/bin/env python

import math


class ObstacleMap(object):
    def __init__(self, map_data, origin_x, origin_y, scale):
        self.map_data = map_data
        self.width = len(map_data[0])
        self.height = len(map_data)
        self.origin_x = origin_x
        self.origin_y = origin_y
        self.scale = scale

    # TODO: check
    def calc_range(self, robot_x, robot_y, robot_a, max_range):
    # def calc_range(self, robot_y, robot_x, robot_a, max_range):
        x0, y0 = self.world_to_map(robot_x, robot_y)
        x1, y1 = self.world_to_map(robot_x + max_range*math.cos(robot_a),
                                   robot_y + max_range*math.sin(robot_a))

        if abs(y1-y0) > abs(x1-x0):
            steep = True
        else:
            steep = False

        if steep:
            x0, y0 = y0, x0
            x1, y1 = y1, x1

        deltax = abs(x1-x0)
        deltay = abs(y1-y0)
        error = 0
        deltaerr = deltay

        x = x0
        y = y0

        if x0 < x1:
            xstep = 1
        else:
            xstep = -1
        if y0 < y1:
            ystep = 1
        else:
            ystep = -1

        while x != (x1 + xstep*1):
            x += xstep
            error += deltaerr
            if 2*error >= deltax:
                y += ystep
                error -= deltax

            # TODO: check
            # if steep:
            if not steep:
                if self.map_valid(y, x):
                    if self.map_data[y][x]:
                        return (math.sqrt((x-x0)*(x-x0) + (y-y0)*(y-y0)) /
                                self.scale)
            else:
                if self.map_valid(x, y):
                    if self.map_data[x][y]:
                        return (math.sqrt((x-x0)*(x-x0) + (y-y0)*(y-y0)) /
                                self.scale)

        return max_range

    # # Convert from map index to world coords
    # def map_to_world(self,i,j):
    #   return (self.origin_x + ((i) - self.width / 2) * self.scale,
    #           self.origin_y + ((i) - self.height / 2) * self.scale)

    # Convert from map index to world coords
    def map_to_world(self, i, j):
        return (self.origin_x + i / self.scale,
                self.origin_y + j / self.scale)

    # Convert from world coords to map coords
    def world_to_map(self, x, y):
        return (int(math.floor((x - self.origin_x) * self.scale)),
                int(math.floor((y - self.origin_y) * self.scale)))

    # # Convert from world coords to map coords
    # def world_to_map(self,x,y):
    #   return (int(math.floor((x - self.origin_x) / self.scale + 0.5) + self.width / 2),
    #           int(math.floor((y - self.origin_y) / self.scale + 0.5) + self.height / 2))

    #Test to see if the given map coords lie within the absolute map bounds.
    def map_valid(self, i, j):
        return ((i >= 0) and (i < self.height) and
                (j >= 0) and (j < self.width))
